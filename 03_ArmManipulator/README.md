# 3. Exercise: Robotic Arm Manipulator

## What is this task about?

This exercise is about to work with robotic arms in simulation and visualization environments like RViz and Gazebo. The exercise is subdivided into two parts:

1. Influence of PID controller and how to tune the PID constants.
2. How to perform pick and place tasks using the MoveIt framework.

> **Learning objective**:
>
> After completion of exercise 3 you should know...
>
>- ... what PID control is.
>- ... which effect the PID constants have on a robot.
>- ... how to perform a pick and place task using the MoveIt framework.

## Task 3.1: Controller Tuning

### <u>Task setup</u>
To visualize which effect a properly tuned PID controller has and how the behavior of the robot is affected by the different terms (**P**roportinal, **I**ntegral, **D**erivative) we will facilitate the `UR5` robot inside of a simulation envrionment in `ROS`. The objective will be to tune the PID constants ($K_P$, $K_I$, $K_D$) of the `UR5` wrist so that overshoot and settling time are reduced. To document  and visualize the progress of the tuning process, you can use the ROS GUI framework `rqt` to plot the reference and measured joint state.

### Installation of necessary packages

Befor we can start with the tuning process of the PID controller, some packages have to be installed inside your docker container:

- Change into the *src* folder of your catkin workspace and download the universal robots package.

    ```bash
        cd ~/catkin_ws/src
    ```

    ```bash
        git clone -b $ROS_DISTRO-devel https://github.com/ros-industrial/universal_robot.git
    ```

- Install *ros_control*. It's a set of packages that include controller interfaces, controller managers, transmission and hardware interfaces. Additionaly, we install *ros_controllers*, which is a library of controllers.

    ```bash
        apt-get update && apt-get install ros-$ROS_DISTRO-ros-control ros-$ROS_DISTRO-ros-controllers
    ```

- Now, after installation of several new packages, it has to be ensured that all dependencies needed for the new installed packages are present on our system. A command line tool to make sure that is the case is *rosdep update* and *rosdep install*.

    ```bash
        rosdep update
    ```

    ```bash
        cd ~/catkin_ws && rosdep install --rosdistro $ROS_DISTRO --ignore-src --from-paths src
    ```

- After installation of all packes we can build our workspace and source it afterwards.

    ```bash
        catkin build
    ```

    ```bash
        source ~/.bashrc
    ```
- The following command prints all environment variables to the terminal that contain *ROS*. The path to the newly created packages should be now present in the environment variable *ROS_PACKAGE_PATH*.

    ```bash
        printenv | grep ROS
    ```

Now all packages are installed that are necessary to work with the `UR5` and tune the PID controller constants.

### Setting up the working environment

Following, the instructions to set up and environment to inspect and document the influence of the PID controller constants $K_P$, $K_I$ and $K_D$ on the performance of the given `UR5` robotic arm manipulator. The [`Gazebo`](https://gazebosim.org/home) simulation envrionment and the `rqt` (gui) framework with it's build in plugins to publish messages to topics, visulaize messages and manipulate (controller) parameters will be used.

- Launch an empty `Gazebo` world with the paused option set to *true*.

    ```bash
        roslaunch gazebo_ros empty_world.launch paused:=true
    ```

    After execution of the command the `Gazebo` GUI opens up. You can inspect it under following link: [http://localhost:8080/vnc.html](http://localhost:8080/vnc.html)

- Spawn the `UR5` model.

    ```bash
        rosrun gazebo_ros spawn_model -file $(rospack find ur5-joint-position-control)/urdf/ur5_jnt_pos_ctrl.urdf -urdf -x 0 -y 0 -z 0.1 -model ur5 -J shoulder_lift_joint -1.5 -J elbow_joint 1.0
    ```

    The graphical representation of the `UR5` robot should now show up in the `Gazebo` GUI.

- Launch the controllers.

    ```bash
        roslaunch ur5-joint-position-control ur5_joint_position_control.launch
    ```

- Unpause the simulation in the `Gazebo` GUI.

Now the `rqt` environment will be set up to interact and visualize the interaction with the robot. `rqt` offers a number of build in plugins for message publication, visualization, manipulation of parameters, etc.

- Open up a new terminal and run the *rqt_gui* node.

    ```bash
        rosrun rqt_gui rqt_gui
    ```
- In the opening window select the *Plugins* tab, go to the *Topics* option and select *Message Publisher*. The *Message Publisher* plugin allows to publish to a certain topic using a GUI. Add the ***/wrist_1_joint_position_controller/command*** by selecting it in the drop down menu and click the green plus sign. Once you add the topic it should look something like shown in Fig 1. In the widget below, the message that should be published to the topic can be set by extending the topic and setting the expression column to the desired value. Start by setting the expression to a static value. To move the robots wrist joint the message expects the rotation to be given in radiant. So set the expression to -1.57 (= $\frac{\pi}{2}$) which is equal to a 90° rotation. As long as the checkbox in the left is checked for a certain topic, the message gets published to the topic. 

| ![topic_pub](images/topic_pub.png) |
|:--:|
| <b>Fig 1</b>|

- Set different values for the wrist joint and see how the joint state gets adjusted in `Gazebo`.

- To visualize the influence of the PID constants, use the rqt plot plugin to plot the reference value and the measured output of the wrist joint position. Select the *Plugins* option, go to the *Visualization* option and select the *Plot* plugin.
Add the ***/wrist_1_joint_position_controller/state/process_value*** topic to which the measured joint position is published to and the ***/wrist_1_joint_position_controller/command/data*** topic to which the reference value for the joint is published to. The plot is now joint position over time. Once you add both these topics you will see a graph and this will show you how the current position of the wrist changes with respect to the set value. 

| ![rqt_plot](images/rqt_plot.png) |
|:--:|
| <b>Fig 2</b>|

- Now make the wrist joint change it's angle by using the Message Publisher (Fig 1) we set up before and see how the measured joint position approaches the reference value. Like shown in the lecture, depending on the PID constants set for that specific joint, the joint approaches the reference state either underdamped, overdamped or critically damped.

- To tune the PID constants, select the *Plugins* tab, go to the *Configuration* option and select the *Dynamic Reconfigure* plugin. Using that plugin it's possible to adjust the PID constants for each specific joint. Open the pid constants for the ***/wrist_1_joint_position_controller*** (indicated in Fig 3).

| ![dynamic_reconfigure](images/dyn_recon.png) |
|:--:|
| <b>Fig 3</b>|

- With that set up, you can now tune your PID constants, change the joint angle using the Message Publisher and plot the reference joint position and the measured joint position. Use that environment to optimize the PID constants to reach the desired behavior.

### <u>Your Task</u>
1. Your task is to tune the PID controller for `wrist_1_joint_position_controller` to make the writs move smoothly to the position that you set using the `/wrist_1_joint_position_controller/command` topic that we set up in **Fig 1**. 

2. As you start tuning the PID parameter you will see the graph (shown in Fig 2) will change showing the response of writs to different wrist values. Your initial graph will look like **Fig 4** (before tuning the PID parameters).

| ![untuned_wrist](images/result.png) |
|:--:|
| <b>Fig 4</b>|

3. **Fig 4** shows how an untuned wrist joint will react to any given position. Your task is to tune the `PID` parameters using the Dynamic Reconfigure(shown in Fig 3) to make the writs move smooth to the set position shown in **Fig 5**

| ![untuned_wrist](images/un_tuned.gif) |
|:--:|
| <b>Fig 5</b>|

| ![tuned_wrist](images/tuned.gif) |
|:--:|
| <b>Fig 6</b>|



## Pick and Place

In the previous section you learnt which effect a controller and its parameters have on an robotic arm. As a next step, we use the robotic arm *Panda* from [Franka Emika](https://www.franka.de/) which comes already with a tuned controller. We will control the simulation of the robot inside the `ROS` environment using the [MoveIt Motion Planning Framework](https://moveit.ros.org/) to perform a simple pick and place task.

- Change into the *src* folder of your `catkin workspace`.

    ```bash
    cd ~/catkin_ws/src
    ```

- Clone the following packages from git.

    ```bash
    git clone https://github.com/ros-planning/moveit_tutorials.git -b melodic-devel 
    ```

    ```bash
    git clone https://github.com/ros-planning/panda_moveit_config.git -b melodic-devel
    ```

- Install now all package dependencies that are missing in your workspace but needed in the just installed packages.

    ```bash
    rosdep update
    ```

    ```bash
    apt update
    ```

    ```bash
    rosdep install -y --from-paths . --ignore-src --rosdistro melodic
    ```

- Also, make sure you have the `task_2` package cloned to your workspace.

- Change into your `catkin workspace` and **build** the packages.

    ```bash
    cd ~/catkin_ws
    ```

    ```bash
    catkin build --jobs 1
    ```

- Source the `catkin workspace`.

    ```bash
    source ~/.bashrc
    ```

- Now launch the panda robot in `RViz`.

    ```bash
    roslaunch task_2 task_visualization.launch
    ```
    If everything worked right, you should get an rviz window with the *Panda arm* robot as seen in the image below.
    ![rviz_with_panda_arm](../00_GettingStarted/docker/i2r/catkin_ws/src/task_2/images/rviz.png)

- You can run an **example of the pick and place task** by running the [one_object_mainpulation.py](../00_GettingStarted/docker/i2r/catkin_ws/src/task_2/scripts/one_object_manipulation.py) code using the following command.
    ```bash
    rosrun task_2 one_object_manipulation.py
    ```
    You will see the following output if everything worked as expected. 
    ![one_obj_png](../00_GettingStarted/docker/i2r/catkin_ws/src/task_2/images/one_obj_ex.png)


    You will also be able to visualize the *panda arm* moving an object from one table to another.\
    <video width="640" height="480" controls>
        <source src="../00_GettingStarted/docker/i2r/catkin_ws/src/task_2/images/one_object_manipulation.mp4" type="video/mp4">
    </video>

## <u>Your Task</u>

Your task is to extend the [one_object_mainpulation.py](../00_GettingStarted/docker/i2r/catkin_ws/src/task_2/scripts/one_object_manipulation.py) code manipulate multiple objects. 

You have to satisfy the following conditions.

1. The tables `table1` (in front of robot) and `table2` (either on the left or right of the robot) should be at 90&deg; to each other.
2. You need to manipulate **3 objects** and move it from `table1` to `table2`.
3. All the 3 objects **should have different sizes**. Make sure that the width of the object is not larger than the gripper widht. Otherwise the robot will not be able to grasp the objects.
4. You are free to choose the where to position the objects on `table1`. You can also drop the objects anywhere on `table2`. The only condition is that you move the objects **from table1 to table2.** We expect to see a result like the following where 3 objects of different size is moved from `table1` to `table2`.

<video width="640" height="480" controls autoplay muted>
        <source src="../00_GettingStarted/docker/i2r/catkin_ws/src/task_2/images/student_task.mp4" type="video/mp4">
</video>

Once quick point to remember is that the robot's movement might be slower due to 2 reasons.
- Your docker doesn't have enough GPU and CPU resources assigned to make the planning and rendering process faster.
- The time required for the algorithm to plan and execute the trajector.

In both these cases, you don't have to worry. It is totally fine as long as you are able to accomplish the task.

You can find further information about using the `MoveIt` framework [here](http://docs.ros.org/en/melodic/api/moveit_tutorials/html/doc/move_group_python_interface/move_group_python_interface_tutorial.html).

[Link to a video explaining the task](pick_place_task_pitch_video.mp4)
