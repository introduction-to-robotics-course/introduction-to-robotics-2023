## 01 Tutorial: Introduction to Python

The task for this tutorial can be found in the [01_Tutorial_PythonIntroduction jupyter notebook](01_Tutorial_PythonIntroduction.ipynb). Following, the steps how to import the jupyter notebook to [jupyter hub](https://jupyter.rwth-aachen.de/hub/login).

1. Download the [01_Tutorial_PythonIntroduction jupyter notebook](https://git.rwth-aachen.de/introduction-to-robotics-course/introduction-to-robotics-2023/-/blob/main/01_IntroductionToPython/01_Tutorial_PythonIntroduction.ipynb).

    ![GIF that shows download of the jupyter notebook](media/download_jupyter_notebook.gif)

2. Open the [RWTH JupyterHub](https://jupyter.rwth-aachen.de/hub/login) website and sign in using RWTH Single Sign-On.

    ![GIF showing sign in process to RWTH jupyterHub](media/login_jupyterHub.gif)

3. Inside RWTH JupyterHub create a new folder named **i2r**.

    ![GIF showing process to create new folder](media/creating_folder.gif)

4. Upload the [01_Tutorial_PythonIntroduction jupyter notebook](01_Tutorial_PythonIntroduction.ipynb) and the [forward_kinematics module](forward_kinematics.py) to the **i2r** folder.

    ![GIF showing process to import files](media/import_files.gif)

5. You can open the file by just double-click the file name in the file browser.

    ![GIF showing how to open files](media/opening_files.gif)

6. To run/execute the code you implemented in a cell, click into that cell and click the ***Run the selected cells and advance*** button like show below.

    ![GIF showing how to run a cell](media/run_a_cell.gif)

7. Once you made changes inside the file **forward_kinematics.py** make sure to save the file (Press `Ctrl` + `S` (on a german keyboard `Strg` + `S`)) and click on ***Restart the kernal, then re-run the whole notebook*** inside of the notebook from which you are calling a function of the module.

    ![GIF showing how to restart the kernal](media/restart_kernal.gif)

