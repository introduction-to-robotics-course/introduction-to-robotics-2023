# Introduction to Python

> **Learning Objectives**
>
> In this session we will be looking at the key concepts in programming using the programming language Python. After this exercise, you will know ...
> - common programming languages used in robotics and their use cases.
> - Pythons basic syntax, variables, data types, programming constructs and modules.
> - how to write a Python function.
> - how to plot data using Python.

## Programming in robotics

Per **definition** "a robot is an autonomous machine capable of sensing its environment, carrying out computations to make decisions, and performing actions in the real world" ([IEEE](https://robots.ieee.org/learn/what-is-a-robot/)). 

The main **hardware components** of robots are *acuators*, *sensors* and a *computational unit*. These three hardware building blocks are needed in order to make a robot that corresponds to the definition running.
But another essential part to make a robot running is its **software**. So programs that have the task to **process** the sensed data and **interpret** it, **take decisions** and make the actuators act accordingly.

So robot programming is basicly programming the computational unit inside a robot to perform for a defined use case using its actuators and information/feedback from the sensors.
The computational unit that runs the software/programs can be seen as the brain of the robot. Programming is therefore of great importance in robotics.

## Programming languages

In robotics many different programming languages are used. Depending on the **focus** of the current robotic project, the differnent programming languages have advantages and disatvantages. 

For projects or tasks where **computation time** is relevant or **low-level programming of hardware** is desired usually `C/C++` is used. 

`Python` is commonly used, when the focus is on **rapid prototyping** or when the goal is **learning robotics programming** with not much background in programming. `Python` is a beginner friendly programming language, as it has an easily readable lanuage, comes with a lot of ready-to-use libraries and is a garbage collecting language, which means that the user doesn't have to take care of memory usage and management.

In scientific research the language `Matlab` is also sometimes used because it's orientation towards **mathematical programming**. That makes it easy to test complex algorithm with less lines of code.

During the `Introduction to Robotics` course we will use `Python` since it's well suited for **beginners** and for **rapid prototyping**.

## Python

We will continue now with an introduction to the basic programming constructs in `Python`. The programming constructs are similar in different languages, so once you learned them in `Python` it will be easier to learn new languages.

### Variables

**Variables** can be described as containers for storing data values.

Variable creation works in `Python` by just assigning a value to it. There is no need in `Python` to declare a variables before using them or declare their type. That's why it's called a **dynamically typed language**.

The naming convention in `Python` is to use lowercase single letter, word or words. If several words should be used, seperate them with an underscore.

**Example**:

```python
variable_name = "variable value" # variable with a value of datatype String (str)
x = 5 # variable with a value of data type Integer (int)
```

### Data Types

Like mentioned in the **Variables** subsection, values assigned to variables have a certain **data type**. `Python` comes with a few built-in data types. The most important ones we will use are listed below:

- data type for **text**: *str*
- data types for **numeric** values: *int*, *float*, *complex*
- data types for **sequences**: *list*, *tuple*
- data type for **mapping**: *dict*
- data type for **Boolean** values: *bool*

To find out the kind of data type a certain variable has can be find out by the following command:

```python
x = 1
print(type(x)) # prints <class 'int'>
y = 2.0
print(type(y)) # prints <class 'float'>
z = "text"
print(type(z)) # prints <class 'str'>
```

Beside these built-in types one can define as well own data types. But more information on that in following appointments.

### Operators

#### Arithmetic operators
If a variable has a numeric data type it's possible to use common mathematical operators on it. Here a few examples:

```python
x = 1.0
y = 2.0
print(x+y) # printed result would be 3
print(x-y) # printed result would be -1
print(x*y) # printed result would be 2
print(x/y) # printed result would be 0.5
```
A table of all built-in operators can be found [here](https://docs.python.org/3/library/operator.html#mapping-operators-to-functions).

#### Relational & logical operators

**Relational** and **logical operators** are needed in nearly every programme to mange the flow of the program. They are used to define **conditions** on which **decision strutures** are then based.

**Relational operators**:

- define a relationship between to operands
- return a boolean value (True or False)
- Operators:
    - \> (greater than)
    - \< (less than)
    - == (equal to)
    - != (not equal to)
    - \>= (greater than or equal to)
    - \<= (less than or equal to)

**Logical operators**:

- used in expressions where the operands are of type bool (so True or False)
- retruns a boolean value (True or False)
- Operators:
    - AND
    - OR
    - NOT

### Decision structures
**Decision structures** are used to evaluate variables, check for conditions and take actions according to the defined condition.
`Python` comes with the following structures of decision making:

- **if** statements
- **if...else** statements
- nested if statements

The statements are executed sequentially.

```python
x = 5
y = 6
if (x>=y):
    print("x is greater than or equal to y")
else:
    print("x is less than y")
```

### Loops

In case you have to continue a process several times or until a certain condition is reached, these process can be executed inside a **loop**. `Python` comes with the following built-in types for loops:

- **while** loop: repeats the statements inside of the loop until the given condition is true
- **for** loop: executes the sequence of statements inside of the loop for a given times
- nested loops

```python
count = 0
while (count < 5):
    count += 1
    print(count)

list_x = range(1,5)

for i in list_x:
    print(i)
```

### Functions
Since several processes in programs have to be done multiple times, it is good practice and saves a lot of time and resources to outsource these processes into **functions**. A **function** is a block of code that is designed to perform a single action and is reusable. To use functions in `Python` brings **modulatrity**  to the project. `Python` comes with a row of built-in functions, but it's as well possible to write your own functions.

```python
def function_name(argument_1, argument_2, argument_n):
    # code that sould be processed when the function is called
    retrun result
```

## Example: Forward Kinematics
Using **Forward Kinematics** as an example, we will introduce the basics of Python we learned in the last sections.

The **forward kinematics** formular for a planar space **2 DoF** elbow manipulator (Lecture - Course Introduction, Slide 44) can be implemented in Python using the *math* module.

$x = a_1 \cdot cos(\theta_1) + a_2 \cdot cos(\theta_1 + \theta_2)$

$y = a_1 \cdot sin(\theta_1) + a_2 \cdot sin(\theta_1 + \theta_2)$

Known parameters from geometry:
- Length of links
    - $a_1$
    - $a_2$

Variables:
- Rotation angle of the joints
    - $\theta_1$
    - $\theta_2$

The programming can be done using the jupyter hub of RWTH. ([Link here](https://jupyter.rwth-aachen.de/hub/login))

## Tutorial Task

Now you got introduced to the fundamentals of `Python` using the example of forward kinematics inside of the jupyter notebooks [i2r_python_intro_fk.ipynb](i2r_python_intro_fk.ipynb) and [i2r_python_intro_fk_with_functions.ipynb](i2r_python_intro_fk_with_functions.ipynb).

Based on that knowledge you should now solve the **tutorial tasks**. You can find the tasks and some additional instructions on how to use the RWTH jupyterHub in the [Tutorial folder](Tutorial/).
