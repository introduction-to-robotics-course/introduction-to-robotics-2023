# This Python file contains different functions we implemented to perform forward kinematics
# 
# First we have to make sure to import the modules with the functions we need for the functions.
# Here we just need the 'math' module which includes among others the following functions:
# - cos(x): returns cosine of x in radians
# - sin(x): returns sine of x in radians
# - radians(x): converts x from degree to radians

import math

# Here we define the function of the name 'forward_kinematics_nLinks_2'.
# For an elbow manipulator in planar space with 2 DoF the function calculates 
# the coordinates of the end effector joint.
# The function has to be called with the follwing arguments:
# - a_1: length of link 1
# - a_2: lenght of link 2
# - theta_1: rotation of joint 1
# - theta_2: rotation of joint 2
# The function returns the x and y coordinate of the end effector joint.
def forward_kinematics_nLinks_2(a_1, a_2, theta_1, theta_2):
    
    x = a_1 * math.cos(math.radians(theta_1)) + a_2 * math.cos(math.radians(theta_1 + theta_2))
    y = a_1 * math.sin(math.radians(theta_1)) + a_2 * math.sin(math.radians(theta_1 + theta_2))
    
    return x, y

# Here we define the function of the name 'forward_kinematics'.
# For an elbow manipulator in planar space with n DoF the function calculates 
# the coordinates of the end effector joint.
# The function has to be called with the follwing arguments:
# - x_0: x coordinate of the refernce coordinate frame
# - y_0: y coordinate of the reference coordinate frame
# - list_a: list with the lenghts [a_1, a_2, ..., a_n] of the links
# - list_theta: list with the rotations [theta_1, theta_2, ..., theta_n] of the joints
# The function returns the x and y coordinate of the end effector joint.
def forward_kinematics(x_0, y_0, list_a, list_theta):
    x = x_0
    y = y_0
    sum_theta = 0
    # The zip() function takes iterables (can be zero or more), aggregates them in a tuple, and returns it.
    for a, theta in zip(list_a, list_theta): 
        sum_theta = sum_theta + theta
        x = x + a * math.cos(math.radians(sum_theta))
        y = y + a * math.sin(math.radians(sum_theta))
        
    return x, y


# Here we define the function of the name 'fk_getJointCoords'.
# For an elbow manipulator in planar space with n DoF the function calculates 
# the coordinates of the joints.
# The function has to be called with the follwing arguments:
# - x_0: x coordinate of the refernce coordinate frame
# - y_0: y coordinate of the reference coordinate frame
# - list_a: list with the lenghts [a_1, a_2, ..., a_n] of the links
# - list_theta: list with the rotations [theta_1, theta_2, ..., theta_n] of the joints
# The function returns two lists. 'joint_x_coords' contains the x coordinates of the joints and
# 'joint_y_coords' contains the y coordinates of the joints.
# This function is used to be able to plot the resulting elbow manipulator configuration.
def fk_getJointCoords(x_0, y_0, list_a, list_theta):
    x = x_0
    y = y_0
    sum_theta = 0
    
    joint_x_coords = [x_0]
    joint_y_coords = [y_0]
    
    # The zip() function takes iterables (can be zero or more), aggregates them in a tuple, and returns it.
    for a, theta in zip(list_a, list_theta): 
        sum_theta = sum_theta + theta
        x = x + a * math.cos(math.radians(sum_theta))
        y = y + a * math.sin(math.radians(sum_theta))
        joint_x_coords.append(x)
        joint_y_coords.append(y)
    
    return joint_x_coords, joint_y_coords
    