# Introduction to Robotics - Summer Term 2023

This repository guides you through the exercise material used in the `Introduction to Robotics` course held at the [Chair of Geotechnical Engineering](https://www.gut.rwth-aachen.de/cms/~krvwh/Geotechnik/?lidx=1). The course is divided into lectures and exercises. 

The lectures cover the theoretical concepts of robotics like 
 - control systems, 
 - kinematics, 
 - task and path planning and 
 - navigation.
 
The lecture material can be found in the RWTHmoodle room of the course.
 
The exercises and tutorials introduce the concepts of the  Robot Operating System (ROS) which is a set of software tools and libraries to build robot applications. The transfer of the learned theoretical concepts to software libraries and their use in ROS will be demonstrated using typical robotic applications, such as a robotic arm (Task 2) and a wheeled mobile robot (Task 3).


## Exercise/Tutorial 1: Introduction to ROS

> **Learning goals**:
>
> After completion of exercise 1 you should know the ROS components and concepts
> - Master,
> - Node,
> - Message,
> - Topic,
> - Service
>
> and how the organization of ROS on a filesystem level in packages works using the catkin build system.

The learning material for exercise 1 can be found in the [exercise_1](exercise_1) directory.

## Exercise/Tutorial 2: Using a robotic arm (PID Controller tuning; Pick and Place using MoveIt)

## Exercise/Tutorial 3: Using a mobile robot
