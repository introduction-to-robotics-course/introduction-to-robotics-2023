---
marp: true
theme: default
_class: invert
author: Jan Holzheim
backgroundColor: white
color: #00549f
header: Introduction to Robotics
footer: 1. Exercise: Introduction to ROS | Jan Holzheim, M. Sc. | 2023
paginate: true
---
<!-- _paginate: false -->
<!-- _color: #00549f-->
<!-- _backgroundColor: white-->
<!-- _-->
# **Introduction to ROS**

Todays Outline

1. What is ROS?
2. ROS Philosophy 
3. ROS Concepts
4. First Steps with ROS
5. Building a Package
6. Implementing a Node

---

# What is ROS?

---
<!-- _backgroundColor: black -->
# ROS Philosophy

---
<!-- _backgroundColor: black -->
# ROS Concepts

---
<!-- _backgroundColor: black -->

# First Steps with ROS

## ROS Workspace

---

# First Steps with ROS

## Catkin Workspace

- *catkin* is a `ROS` build system to generate executables, libraries and interfaces.
- In this course, we'll use the *Catkin Command Line Tools*.
- To build your workspace, for exeample after implementation of a node in a package, use the *catkin build* command from within the catkin workspace.

    ```bash
    cd ~/catkin_ws
    ```

    ```bash
    catkin build
    ```

---

## Catkin Workspace (cont.)

- Inspect the created folder and file structure in your workspace by using the *ls* command

    ```bash
    ls
    ```

- Overlay this worspace on top of your ROS environment and source your *bashrc* file 

    ```bash
    echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
    ```

    ```bash
    source ~/.bashrc
    ```
---

## Building a Package

Since the *catkin* workspace is now set up, we can build a package in it.

- Change int the *src* folder of the catkin workspace

    ```bash
    cd ~/catkin_ws/src
    ```

- The tool to build a package is *catkin_create_pkg*

    ```bash
    catkin_create_pkg <package name> <dependencies>
    ```

---

## Building a Package (cont.)

- We name our first package *exercise_1* and use it to implement our first nodes to gain experience with the components building the `ROS` computation graph.

    ```bash
    catkin_create_pkg exercise_1 std_msgs rospy roscpp
    ```

- Since, we will use the `Python` programming language, it's common practice to store the python scripts into a seperate *scripts* folder and not into the *src* folder, which is mainly used for `C/C++` source code.

    ```bash
    cd ~/catkin_ws/src/exercise_1
    ```

    ```bash
    mkdir scripts
    ```

---

## Building a Package (cont.)

- Following the creation of the package, change directory into the *catkin_ws* folder to build your package.

    ```bash
    cd ~/catkin_ws
    ```

    ```bash
    catkin build
    ```

---

## Building a Package (cont.)

- After the build process make sure to source the configuration file for the terminal (*bashrc*) to ensure that the updated environment parameters are used.

    ```bash
    source ~/.bashrc
    ```

- Check if the just created package *exercise_1* is added to the *ROS_PACKAGE_PATH* **environment variable**

    ```bash
    echo $ROS_PACKAGE_PATH
    ```

---

## Implementing a Node

Inside the package *exercise_1* we will now create a `node` that **publishes** `messages` to the *cmd_vel* `topic` which is used for example in the *turtlesim* `package`.

- Create the empty *turtle_go.py* file in the just created *scripts* folder inside of the `package` *exercise_1*.

    ```bash
    touch ~/catkin_ws/src/exercise_1/scripts/turtle_go.py
    ```

- The implementation shown on the next slide.

---

```python
#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist

def turtle_go():
    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    rospy.init_node('turtle_go', anonymous=True)
    rate = rospy.Rate(10) # 10Hz
    twist = Twist()
    twist.linear.x = 1.0
    while not rospy.is_shutdown():
        pub.publish(twist)
        rate.sleep()
        
if __name__ == '__main__':
    try:
        turtle_go()
    except rospy.ROSInterruptException:
        pass
```

---

## Implementing a Node (cont.)

- Make the file executable by the following command giving it read, write and execute permissions.

    ```bash
    chmod 7 turtle_go.py
    ```
