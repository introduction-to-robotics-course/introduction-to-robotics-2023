# Introduction to ROS

> **Learning objective**:
>
> After completion of exercise 1 you should know the ROS components and concepts
> - Master,
> - Node,
> - Message,
> - Topic,
> - Service
>
> and how the organization of ROS on a filesystem level in packages works using the catkin build system.

## Sections of this Exercise

1. [What is ROS?](#what-is-ros)
2. [ROS Philosophy](#ros-philosophy)
3. [ROS Concepts](#ros-concepts)
    1. [Computation Graph Level](#computation-graph-level)
    2. [Filesystem Level](#filesystem-level)
    3. [Community Level](#community-level)
4. [First Steps with ROS](#first-steps-with-ros)
    1. [ROS Workspace](#ros-workspace)
    2. [ROS Master](#ros-master)
    3. [Nodes](#nodes)
    4. [Topics](#topics)
    5. [Services](#services)
    6. [Messages](#messages)
    7. [ROS CLI commands](#ros-command-line-interface-commands)
    8. [Catkin Workspace](#catkin-workspace)
5. [Building a Package](#building-a-package)
6. [Implementing a Node](#implementing-a-node)


## What is ROS?


## ROS Philosophy


## ROS Concepts

### Computation Graph Level

### Filesystem Level

### Community Level



## First Steps with ROS

Now we can **start** `Docker` to run the environment we use to work with `ROS`. The instructions how to start the group of `Docker` containers can be found in the [00_GettingStarted](/00_GettingStarted/README.md#docker) folder.

During this course we work with two containers. A short description on their purpose in the table below.

| Container Name | Intent |
| :-----: | :----- |
| ros | Container with *Ubuntu 18.04* as operating system and the `ROS` distribution *Melodic* installed. This container will host our `ROS` environment. Since docker is designed to containerise command-line interface applications, it is not optimised to facilitate graphical user interfaces (GUIs). As a workaround, we make use of the auxiliary container *gui*. |
| gui | Auxiliary container to work with graphical user interfaces (GUI). This container displays GUI applications from the *ros* container inside a web application.|


### ROS Workspace


### ROS Master

### Nodes

### Topics

### Services

### Messages

### ROS command-line interface commands

### Catkin Workspace

- ***catkin*** is a `ROS` build system to generate executables, libraries and interfaces
- We will use the *Catkin Command Line Tools* within this course.
- To build your workspace after implementation, e.g. of a node, use the *catkin build* command from within the catkin workspace

    ```bash
    cd ~/catkin_ws
    ```

    ```bash
    catkin build
    ```

- Inspect the created folder and file structure in your workspace by using the *ls* command

    ```bash
    ls
    ```

- Overlay this worspace on top of your ROS environment and source your *bashrc* file 

    ```bash
    echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
    ```
    ```bash
    source ~/.bashrc
    ```

## Building a Package
Since the *catkin* workspace is now set up, we can build a package in it.

- Change int the *src* folder of the catkin workspace

    ```bash
    cd ~/catkin_ws/src
    ```
- The tool to build a package is *catkin_create_pkg*
    ```bash
    catkin_create_pkg <package name> <dependencies>
    ```
- We name our first package *exercise_1* and use it to implement our first nodes to gain experience with the components building the `ROS` computation graph.
    ```bash
    catkin_create_pkg exercise_1 std_msgs rospy roscpp
    ```
- Since, we will use the `Python` programming language, it's common practice to store the python scripts into a seperate *scripts* folder and not into the *src* folder, which is mainly used for `C/C++` source code.
    ```bash
    cd ~/catkin_ws/src/exercise_1
    ```
    ```bash
    mkdir scripts
    ```
- Following the creation of the package, change directory into the *catkin_ws* folder to build your package.
    ```bash
    cd ~/catkin_ws
    ```
    ```bash
    catkin build
    ```
- After the build process make sure to source the configuration file for the terminal (*bashrc*) to ensure that the updated environment parameters are used.
    ```bash
    source ~/.bashrc
    ```
- Check if the just created package *exercise_1* is added to the *ROS_PACKAGE_PATH* **environment variable**
    ```bash
    echo $ROS_PACKAGE_PATH
    ```


## Implementing a Node
Inside the package *exercise_1* we will now create a `node` that **publishes** `messages` to the *cmd_vel* `topic` which is used for example in the *turtlesim* `package`.

- Create the empty *turtle_go.py* file in the just created *scripts* folder inside of the `package` *exercise_1*.
    ```bash
    touch ~/catkin_ws/src/exercise_1/scripts/turtle_go.py
    ```
- The implementation shown below.
    ```python
    #!/usr/bin/env python
    import rospy
    from geometry_msgs.msg import Twist

    def turtle_go():
        pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
        rospy.init_node('turtle_go', anonymous=True)
        rate = rospy.Rate(10) # 10Hz
        twist = Twist()
        twist.linear.x = 1.0
        while not rospy.is_shutdown():
            pub.publish(twist)
            rate.sleep()
            
    if __name__ == '__main__':
        try:
            turtle_go()
        except rospy.ROSInterruptException:
            pass
    ```

- Make sure to make the file executable by the following command giving it read, write and execute permissions.
    ```bash
    chmod 7 turtle_go.py
    ```


## Task

Create a `package` *task_1* which contains a `node` named *turtle_go_circle.py* which **publishes** commands in such a way to the `topic` *cmd_vel* that the turtle moves in a circle.