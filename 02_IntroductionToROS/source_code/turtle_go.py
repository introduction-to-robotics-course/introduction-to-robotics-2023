#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist

def turtle_go():
    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    rospy.init_node('turtle_go', anonymous=True)
    rate = rospy.Rate(10) # 10Hz
    twist = Twist()
    twist.linear.x = 1.0
    while not rospy.is_shutdown():
        pub.publish(twist)
        rate.sleep()
        
if __name__ == '__main__':
    try:
        turtle_go()
    except rospy.ROSInterruptException:
        pass