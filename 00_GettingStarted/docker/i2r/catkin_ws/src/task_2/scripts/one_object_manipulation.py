#!/usr/bin/env python

import sys
import copy
import math
import rospy
import moveit_commander
import geometry_msgs.msg
import moveit_msgs.msg
from trajectory_msgs.msg import JointTrajectoryPoint
import tf 
from math import pi

def add_scene_object(obj_scene, obj_frame, obj_id, pose, obj_size):
    object_pose = geometry_msgs.msg.PoseStamped()
    object_pose.header.frame_id = obj_frame #defining the frame in which the object's pose is defined
    object_pose.pose.position.x = pose[0]
    object_pose.pose.position.y = pose[1]
    object_pose.pose.position.z = pose[2]

    quat_angle = tf.transformations.quaternion_from_euler(math.radians(pose[3]), math.radians(pose[4]), math.radians(pose[5]))
    object_pose.pose.orientation.x = quat_angle[0]
    object_pose.pose.orientation.y = quat_angle[1]
    object_pose.pose.orientation.z = quat_angle[2]
    object_pose.pose.orientation.w = quat_angle[3]
    obj_scene.add_box(obj_id, object_pose, obj_size)
    rospy.loginfo("Added " + obj_id + " to the scene!")


def pick_action(robot_group, ref_frame, obj_id, obj_pose, obj_size):
    
    pick_locations = [] #defining the list of items to be picked
    
    # Documentation of message definition:  https://docs.ros.org/en/melodic/api/moveit_msgs/html/msg/Grasp.html
    pick_obj = moveit_msgs.msg.Grasp()
    pick_obj.id = "grasp_"+obj_id
    pick_obj.allowed_touch_objects.append(obj_id)

    wrist_to_tcp = 0.103 #distance between the wrist and the Tool Center Point (TCP)
    # Setting grasp pose
    pick_obj.grasp_pose.header.frame_id = ref_frame
    pick_obj.grasp_pose.pose.position.x = obj_pose[0]
    pick_obj.grasp_pose.pose.position.y = obj_pose[1]
    pick_obj.grasp_pose.pose.position.z = obj_pose[2] + wrist_to_tcp
    # quaternion = tf.transformations.quaternion_from_euler(math.radians(obj_pose[3]), math.radians(obj_pose[4]), math.radians(obj_pose[5]))
    quaternion = tf.transformations.quaternion_from_euler(math.radians(180), 0, math.radians(-45)) #This is the angle between the frame "panda_link0" & "panda_hand_tcp"
    pick_obj.grasp_pose.pose.orientation.x = quaternion[0]
    pick_obj.grasp_pose.pose.orientation.y = quaternion[1]
    pick_obj.grasp_pose.pose.orientation.z = quaternion[2]
    pick_obj.grasp_pose.pose.orientation.w = quaternion[3]

    # Setting pre-grasp approach
    pick_obj.pre_grasp_approach.direction.header.frame_id = ref_frame
    pick_obj.pre_grasp_approach.direction.vector.z = -1.0
    pick_obj.pre_grasp_approach.min_distance = 0.01
    pick_obj.pre_grasp_approach.desired_distance = 0.1

    # Setting post-grasp retreat
    pick_obj.post_grasp_retreat.direction.header.frame_id = ref_frame
    pick_obj.post_grasp_retreat.direction.vector.z = 1.0
    pick_obj.post_grasp_retreat.min_distance = 0.15
    pick_obj.post_grasp_retreat.desired_distance = 0.25

    # Setting end effector finger before grasp
    pick_obj.pre_grasp_posture.header.frame_id = ref_frame
    pick_obj.pre_grasp_posture.joint_names = ["panda_finger_joint1","panda_finger_joint2"]
    pos_open = JointTrajectoryPoint()
    pos_open.positions.append(float(0.06))
    pick_obj.pre_grasp_posture.points.append(pos_open)

    #### Setting end effector finger after grasp
    pick_obj.grasp_posture.header.frame_id = ref_frame
    pick_obj.grasp_posture.joint_names = ["panda_finger_joint1","panda_finger_joint2"]
    pos_close = JointTrajectoryPoint()
    pos_close.positions.append(float(obj_size[1]/2))
    pick_obj.grasp_posture.points.append(pos_close)

    pick_locations.append(pick_obj)

    rospy.loginfo("Execting pick action for "+obj_id)
    robot_group.pick(obj_id, pick_locations)
    robot_group.stop()

def place_action(robot_group, ref_frame, obj_id, drop_pose):
    place_location = []
    place_obj = moveit_msgs.msg.PlaceLocation()

    quaternion = tf.transformations.quaternion_from_euler(0, 0, math.radians(135))#This is the angle between the frame "panda_link0" & "panda_hand_tcp"
    place_obj.place_pose.pose.orientation.x = quaternion[0]
    place_obj.place_pose.pose.orientation.y = quaternion[1]
    place_obj.place_pose.pose.orientation.z = quaternion[2]
    place_obj.place_pose.pose.orientation.w = quaternion[3]

    # Setting drop pose
    place_obj.place_pose.header.frame_id = ref_frame
    place_obj.place_pose.pose.position.x = drop_pose[0]
    place_obj.place_pose.pose.position.y = drop_pose[1]
    place_obj.place_pose.pose.position.z = drop_pose[2]


    # Setting pre-grasp approach
    place_obj.pre_place_approach.direction.header.frame_id = ref_frame
    place_obj.pre_place_approach.direction.vector.z = -1.0
    place_obj.pre_place_approach.min_distance = 0.01
    place_obj.pre_place_approach.desired_distance = 0.1

    # Setting post-grasp retreat
    place_obj.post_place_retreat.direction.header.frame_id = ref_frame
    place_obj.post_place_retreat.direction.vector.z = 1.0
    place_obj.post_place_retreat.min_distance = 0.15
    place_obj.post_place_retreat.desired_distance = 0.25

    # Setting posture of eef before grasp
    place_obj.post_place_posture.header.frame_id = ref_frame
    place_obj.post_place_posture.joint_names = ["panda_finger_joint1","panda_finger_joint2"]
    pos_open = JointTrajectoryPoint()
    pos_open.positions.append(float(0.06))
    place_obj.post_place_posture.points.append(pos_open)

    #### Grasping
    place_location.append(place_obj)

    rospy.loginfo("Execting place action for "+obj_id)
    robot_group.place(obj_id, place_location)
    
    robot_group.stop()

if __name__ == "__main__":

    #### Initialise a robotics node
    rospy.loginfo("--- Starting initialising a robotics node ---")
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('one_object_manipulation', anonymous=True)

    #### MoveIt APIs initialising
    rospy.loginfo("--- MoveIt APIs initialising ---")
    scene = moveit_commander.PlanningSceneInterface()
    robot = moveit_commander.RobotCommander()

    group_name = "panda_arm"

    #Robot threshold parameters. DON'T CHANGE THESE UNLESS YOU WANT TO TEST.
    # API documentation of the following parameters - http://docs.ros.org/en/jade/api/moveit_commander/html/classmoveit__commander_1_1move__group_1_1MoveGroupCommander.html#a51bc4850dbeecbe086a0d1e90c2981b6
    move_group = moveit_commander.MoveGroupCommander(group_name)
    move_group.set_goal_position_tolerance(1E-2)
    move_group.set_goal_orientation_tolerance(1E-3)
    move_group.set_num_planning_attempts(10)
    move_group.set_planning_time(15)
    move_group.set_planner_id("RRTConnectkConfigDefault")

    #### Details
    rospy.logwarn("--- Fetching Information about the robot ---")
    planning_frame = move_group.get_planning_frame()
    rospy.loginfo('Planning frame: '+ planning_frame)
    eef_link = move_group.get_end_effector_link()
    rospy.loginfo("End effector link: "+ eef_link)

    #### Publisher for publishing the trajectories
    rospy.logwarn("--- Creating Publisher to publish trajectories ---")
    display_trajectories_publisher = rospy.Publisher("/move_group/display_planned_path", moveit_msgs.msg.DisplayTrajectory, queue_size=20)

    #### Defining Objects
    
    table_ground = 0.3 #height of table from_ground

    #object_id defines the name of the object
    #object_size defined in meter. Format [x, y, z] 
    #object_pose defined in meters and angles in degree. Format [x, y, z, roll, pitch, yaw]

    table1_id = 'table1'
    table1_size = [0.2, 0.5, 0.01]
    table1_pose = [0.5, 0, (table_ground + table1_size[2]/2), 0, 0, 0]

    ## YOUR TAKS
    ## Update the pose of tabl2 so that it is at 90 degree w.r.t table1 
    table2_id = 'table2'
    table2_size = [0.2, 0.5, 0.01]
    table2_pose = [-0.5, 0, (table_ground + table1_size[2]/2), 0, 0, 0]
    

    box1_id = 'box1'
    box1_size = [0.1, 0.05, 0.05]
    box1_pose = [0.5, 0, (table_ground + table1_size[2] + box1_size[2]/2), 0, 0, 0]
    
    ## YOUR TASK
    ## Define two more objects of different sizes and different location on table 1

    #Removing objects if already present (This is done to make sure the tables are respawned when you rerun the code)
    scene.remove_world_object("table1")
    scene.remove_world_object("table2")
    scene.remove_world_object("box1")
    ## YOUR TASK
    ## Remove objects if they are already present in the world.

    #adding objects to the scene.
    add_scene_object(scene, planning_frame, table1_id, table1_pose, table1_size)
    add_scene_object(scene, planning_frame, table2_id, table2_pose, table2_size)
    add_scene_object(scene, planning_frame, box1_id, box1_pose, box1_size)
    ## YOUR TASK
    ## Add your new objects to the scene.

    rospy.logwarn("Objects spawned")
    rospy.sleep(2)
    
    #Defining the pick-place action for the various objects in the scene
    box1_pick_pose = [0.5, 0, (table_ground + table1_size[2] + box1_size[2]), 0, 0, 0]
    box1_drop = [-0.5, 0, box1_pose[2]]
    
    pick_action(move_group, planning_frame, box1_id, box1_pick_pose, box1_size)
    place_action(move_group, planning_frame, box1_id, box1_drop)

    ## YOUR TASK
    ## Recursively pick and place the new objects that you've created.