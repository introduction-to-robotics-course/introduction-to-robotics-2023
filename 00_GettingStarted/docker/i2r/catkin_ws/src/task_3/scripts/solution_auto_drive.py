#!/usr/bin/env python

# Task 3


import rospy 
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
import math

def turn_dir(dir):
    move.linear.x = 0.0
    move.angular.z = dir*0.1
    pub.publish(move)

def drive_straight():
    move.linear.x = 0.15
    move.angular.z = 0.0
    pub.publish(move)

def callback(dt):
    print ('-------------------------------------------')
    print ('Range data at 45 deg:   {}'.format(dt.ranges[45]))
    print ('Range data at 90 deg:  {}'.format(dt.ranges[90]))
    print ('-------------------------------------------')

    dist_left = dt.ranges[90]
    dist_diag = dt.ranges[25]
    low_dist_thresh = 0.45
    high_dist_thresh = 0.65


    if(dist_left < low_dist_thresh and dist_diag < low_dist_thresh*math.cos(math.radians(25))):
        turn_dir(-1)
    elif(dist_left > high_dist_thresh and dist_diag > high_dist_thresh*math.cos(math.radians(25))):
        turn_dir(+1)
    else:
        drive_straight()

    

move = Twist() # Creates a Twist message type object

rospy.init_node('obstacle_avoidance_node') # Initializes a node
pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1000)  # Publisher object which will publish "Twist" type messages
                            				 # on the "/cmd_vel" Topic, "queue_size" is the size of the
                                                         # outgoing message queue used for asynchronous publishing

sub = rospy.Subscriber("/scan", LaserScan, callback)  # Subscriber object which will listen "LaserScan" type messages
                                                      # from the "/scan" Topic and call the "callback" function
						      # each time it reads something from the Topic

rospy.spin() # Loops infinitely until someone stops the program execution