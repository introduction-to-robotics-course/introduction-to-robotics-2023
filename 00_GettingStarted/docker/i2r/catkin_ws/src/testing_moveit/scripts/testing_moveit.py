#!/usr/bin/env python

import sys
import copy
import math
import rospy
import moveit_commander
import geometry_msgs.msg
import moveit_msgs.msg
from trajectory_msgs.msg import JointTrajectoryPoint
import tf 
from math import pi

print("--- Imports done! ---")

#### Initialise a robotics node
print("--- Starting initialising a robotics node ---")
moveit_commander.roscpp_initialize(sys.argv)
print("Executed: moveit_commander.roscpp_initialize(sys.argv)")
rospy.init_node('testing_moveit_node', anonymous=True)
print("Executed: rospy.init_node('testing_moveit_node', anonymous=True)")

#### MoveIt APIs initialising
print("--- MoveIt APIs initialising ---")
scene = moveit_commander.PlanningSceneInterface()
print("Executed: scene = moveit_commander.PlanningSceneInterface()")
robot = moveit_commander.RobotCommander()
print("Executed: robot = moveit_commander.RobotCommander()")
group_name = "panda_arm"
move_group = moveit_commander.MoveGroupCommander(group_name)
print("Executed = move_group = moveit_commander.MoveGroupCommander(group_name)")
move_group_gripper = moveit_commander.MoveGroupCommander("panda_hand")
print("Executed: move_group_gripper = moveit_commander.MoveGroupCommander(\"panda_hand\")")

#### Open and close gripper
print("--- Open and close gripper ---")
move_group_gripper.set_named_target("close")
print("Executed: move_group_gripper.set_named_target(\"close\")")
rospy.sleep(5)
move_group_gripper.go()
print("Exectued: move_group_gripper.go()")
rospy.sleep(5)
move_group_gripper.set_named_target("open")
print("Executed: move_group_gripper.set_named_target(\"open\")")
rospy.sleep(5)
move_group_gripper.go()
print("Exectued: move_group_gripper.go()")
rospy.sleep(5)
print("Executed: rospy.sleep(5)")
rospy.sleep(5)

#### Details
print("--- Getting Information about the robot ---")
group_names = robot.get_group_names()
print('Robot groups available:',group_names)
planning_frame = move_group.get_planning_frame()
print('Reference frame:', planning_frame)
eef_link = move_group.get_end_effector_link()
print("End effector link:", eef_link)
rcs = robot.get_current_state()
print('Robot current state:', rcs)
rospy.sleep(5)

#### Publisher for publishing the trajectories
print("--- Creating Publisher to publish trajectories ---")
display_trajectories_publisher = rospy.Publisher("/move_group/display_planned_path", moveit_msgs.msg.DisplayTrajectory, queue_size=20)
print("Executed: display_trajectories_publisher = rospy.Publisher(\"/move_group/display_planned_path\", moveit_msgs.msg.DisplayTrajectory, queue_size=20)")
rospy.sleep(5)
print('Planning scene objects:', scene.get_objects())
scene.remove_world_object("table")
scene.remove_world_object("table2")
scene.remove_world_object("object1")
scene.remove_world_object("box1")
rospy.sleep(5)

#### Defining Objects

# object IDs
table_id = 'table'
box1_id = 'box1'
target_id = 'target'
tool_id = 'tool'

table_ground = 0.3
table_size = [0.2, 0.7, 0.01]
box_1_size = [0.1, 0.05, 0.05]
target_size = [0.02, 0.01, 0.12]

##### Placing objects

# Table
table_pose = geometry_msgs.msg.PoseStamped()
table_pose.header.frame_id = planning_frame
table_pose.pose.position.x = 0.5
table_pose.pose.position.y = 0
table_pose.pose.position.z = (table_ground + table_size[2]/2)
table_pose.pose.orientation.w = 1.0
scene.add_box(table_id, table_pose, table_size)
print("Added " + table_id + " to the scene!")

# Box
box_pose = geometry_msgs.msg.PoseStamped()
box_pose.header.frame_id = planning_frame
box_pose.pose.position.x = 0.5
box_pose.pose.position.y = 0
box_pose.pose.position.z = (table_ground + table_size[2] + box_1_size[2]/2)
box_pose.pose.orientation.w = 1.0
scene.add_box(box1_id, box_pose, box_1_size)
print("Added " + box1_id + " to the scene!")

rospy.sleep(5)

#### Grasp

# Documentation of message definition:  https://docs.ros.org/en/melodic/api/moveit_msgs/html/msg/Grasp.html

g = moveit_msgs.msg.Grasp()

g.id = "grasp_box1"

# defining the grasp pose
grasp_pose = geometry_msgs.msg.PoseStamped()
grasp_pose.header.frame_id = planning_frame
grasp_pose.pose.position.x = -0.5
grasp_pose.pose.position.y = 0
grasp_pose.pose.position.z = 0.55
quaternion = tf.transformations.quaternion_from_euler(math.radians(180), 0, math.radians(135))
print("-----------------------------------------------")
print("Quaternion: ",quaternion)
print("-----------------------------------------------")
grasp_pose.pose.orientation.x = quaternion[0]
grasp_pose.pose.orientation.y = quaternion[1]
grasp_pose.pose.orientation.z = quaternion[2]
grasp_pose.pose.orientation.w = quaternion[3]
#grasp_pose.pose.orientation.x = 0.924
#grasp_pose.pose.orientation.y = -0.383
#grasp_pose.pose.orientation.z = 0
#grasp_pose.pose.orientation.w = 0



print("Grasp pose z-position = "+str((table_ground + table_size[2] + box_1_size[2]/2)))


move_group.set_planning_time(15)

joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -pi/4
joint_goal[2] = 0
joint_goal[3] = -pi/2
joint_goal[4] = 0
joint_goal[5] = pi/3
joint_goal[6] = 0

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()

#move_group.set_goal_tolerance(0.2)
move_group.set_goal_position_tolerance(1E-2)
move_group.set_goal_orientation_tolerance(1E-3)
move_group.set_num_planning_attempts(5)
move_group.set_planning_time(10)
move_group.set_planner_id("RRTstar")

print("-----------------------------------------------")
print("Known constraints: ",move_group.get_known_constraints())
print("-----------------------------------------------")
print("Path constraints: ",move_group.get_path_constraints())
print("-----------------------------------------------")
print("Plan: ",move_group.plan())
print("-----------------------------------------------")

move_group.set_pose_target(grasp_pose)
plan = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

print("-----------------------------------------------")
print("Current Pose: ",move_group.get_current_pose().pose)
print("-----------------------------------------------")
print("Desired Pose: ", grasp_pose)
print("-----------------------------------------------")

print("Defining Grasp")
grasps = []

g = moveit_msgs.msg.Grasp()
g.allowed_touch_objects.append("box1")
g.allowed_touch_objects.append("table")


# Setting grasp pose
#grasp_pose = geometry_msgs.msg.PoseStamped()
g.grasp_pose.header.frame_id = planning_frame
g.grasp_pose.pose.position.x = 0.5
g.grasp_pose.pose.position.y = 0
g.grasp_pose.pose.position.z = (table_ground + table_size[2] + box_1_size[2] + 0.058 + 0.1)
quaternion = tf.transformations.quaternion_from_euler(math.radians(180), 0, math.radians(135))
g.grasp_pose.pose.orientation.x = quaternion[0]
g.grasp_pose.pose.orientation.y = quaternion[1]
g.grasp_pose.pose.orientation.z = quaternion[2]
g.grasp_pose.pose.orientation.w = quaternion[3]


# Setting pre-grasp approach
g.pre_grasp_approach.direction.header.frame_id = planning_frame
g.pre_grasp_approach.direction.vector.z = -1.0
g.pre_grasp_approach.min_distance = 0.01
g.pre_grasp_approach.desired_distance = 0.1

# Setting post-grasp retreat
g.post_grasp_retreat.direction.header.frame_id = planning_frame
g.post_grasp_retreat.direction.vector.x = 1.0
g.post_grasp_retreat.min_distance = 0.15
g.post_grasp_retreat.desired_distance = 0.25

# Setting posture of eef before grasp
g.pre_grasp_posture.header.frame_id = planning_frame
g.pre_grasp_posture.joint_names = ["panda_finger_joint1","panda_finger_joint2"]
pos_open = JointTrajectoryPoint()
pos_open.positions.append(float(0.06))
g.pre_grasp_posture.points.append(pos_open)


#### set the grasp posture
g.grasp_posture.header.frame_id = planning_frame
g.grasp_posture.joint_names = ["panda_finger_joint1","panda_finger_joint2"]
pos_close = JointTrajectoryPoint()
pos_close.positions.append(float(0.00))
g.grasp_posture.points.append(pos_close)

print("g: ",g)

#### Grasping
grasps.append(g)

print("-----------------------------------------------")
print("Executing Pick task")
move_group.pick("box1", grasps)