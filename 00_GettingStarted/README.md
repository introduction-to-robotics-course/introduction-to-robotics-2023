# Getting started

In this section you will find information about the **software** and **tools** we will use during the course and how to **install** them and how to **set everything up**.

## **I**ntegrated **D**evelopment **E**nvironment (**IDE**)

### What is an IDE?

An *IDE* is a **software application** that provides facilities for software development, like a **source code editor**, **build automation**, **debugger**, **syntax highlighting**, **auto completion** etc.

### Which IDE do we use?

For `Introduction to Robotics` we suggest using the free to use *IDE* `Visual Studio Code` from *Microsoft*. Detailed **installation instructions** based on your OS and links to download the installer can be found under the following links:

- [Windows](https://code.visualstudio.com/docs/setup/windows)
- [Linux](https://code.visualstudio.com/docs/setup/linux)
- [macOS](https://code.visualstudio.com/docs/setup/mac)

In the following subsection we introduce you to the user interface and give a general introduction on how to use `VS Code`. If you are already familiar with `VS Code` feel free to skip the following subsection.

### Getting Started with VS Code

Once the installation worked successfully open `VS Code`. The graphical user interface (GUI) opens up and should look similar to the following image.

![VSC GUI](images/vsc_starting-window.png)*`Visual Studio Code` starting window.*

To get an orientation on how to use and navigate in `VS Code` we recommend you watching the [Getting Started Video](https://code.visualstudio.com/docs/getstarted/introvideos) out of the introductory video series from the `VS Code` website.

### Recommended Extensions for VS Code

The following extensions will be helpful for the upcoming work during the course. In the brackets next to the general name the identifier is stated, which makes it easier to search for the correct extension. Information how to install an extension in `VS Code` can be found in the video mentioned in the section above.

- Python (ms-python.python)
- Pylance (ms-python.vscode-pylance)
- Docker (ms-azuretools.vscode-docker)
- C/C++ Extension Pack (ms-vscode.cpptools-extension-pack)

## Docker

### What is Docker?

`Docker` is a *software tool* that allows developers to package their applications into portable and self-sufficient containers.

Think of it like a shipping container that can hold all the necessary components of an application:

- operating system,
- libraries,
- dependencies and
- code.

This container can then be moved between different environments, such as from a developer's laptop to co-workers laptops or to a server in the cloud, without needing to worry about the underlying infrastructure.

In our case, it will help us to ensure we are all working in the same environment, regardless of your personal machines.

A `Docker container` is a **running instance of a `Docker image`**. It is a standalone executable package that includes everything needed to run the application. Docker container run isolated from the host system and other containers.

A `Docker image` is a **snapshot of an application or system** that has been packaged with all the necessary dependencies and configurations required to run and work for a desired use case. It can be thought of a template or a blueprint for creating a `Docker container`.

In `Introduction to Robotics` we will provide you a `Docker image` that is a snapshot of an **Ubunutu operating system** with the **Robot Operating System (ROS)** installed and all the necessary additional libraries that will be necessary for this course.

In the following sections you will find information on how to install `Docker` and how to start a running `Docker container` as an instance of the `Docker image` we provided for this course.

### Installing Docker

Comprehensive instructions on how to **install** `Docker Desktop` on your machines operating system can be found [here](https://docs.docker.com/get-docker/).

After successfull installation, **open** `Docker Desktop`. The GUI should open as shown in the following screenshot.

![Docker starting window](images/docker_starting-window.png)*`Docker Desktop` starting window*

### Starting a Container to work with ROS

To make sure you can all start with the same working `ROS` environment we use a predefined *image* and *compose* file. Both are stored in this repository at [00_GettingStarted/docker/i2r/](docker/i2r/).

Follow the instructions to download the repository to your local machine and start the group of containers:

1. Create a folder named ***i2r*** for the contents of the `Introduction to Robotics` course.
2. Open this folder in `VS Code` by either starting `VS Code` from Terminal or by using the GUI.
    - Using GUI:
        - Open `VS Code`.
        - Go to *File* &rarr; *Open Folder* and select in the upcoming window the folder *your_local_path/i2r/*
    - Terminal (Linux, macOS):

        ```bash
        cd <your local path>/i2r/
        ```

        ```bash
        code .
        ```

    - Powershell (Windows)

        ```bash
        cd <your local path>\i2r\
        ```

        ```bash
        code .
        ```

3. Open the *Terminal* in `VS Code` (*Terminal* &rarr; *New Terminal*). The path in the Terminal should be the one you selected before.
4. Now clone the repository from gitlab by copying the follwing command into the Terminal and press enter. (You are asked to enter your gitlab user name and password.)

    ```bash
    git clone https://git.rwth-aachen.de/introduction-to-robotics-course/introduction-to-robotics-2023.git
    ```

    - If you don't have *git* installed, you can either install it or go to the gitlab repository using the browser and download the source code into the *i2r* folder.
        - Information on how to install git can be found for all OS [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
5. Now that all the files from the repository are on your local machine, change in the terminal to the folder where the *docker-compose.yml* file is stored.

    ```bash
    cd introduction-to-robotics-2023/00_GettingStarted/docker/i2r/
    ```

6. Run the following command to spin up the scenario that is defined in the [docker-compose.yml](docker/i2r/docker-compose.yml) file. To be able to use all commands starting with *docker*, `Docker Desktop` has to run in the background.

    ```bash
    docker compose up
    ```

    This command will download the needed images and start two containers. One container named ***i2r-ros-1*** which contains a snapshot of an Ubuntu OS with ROS and additional packages installed. The second container is an auxiliary container named ***i2r-gui-1*** that will be used to inspect graphical user interfaces of applications started in the i2r-ros-1 container.

    The terminal output will look the following:

    ![Terminal Output for *docker compose up*](images/docker_compose_up_terminal.png)

7. In the `Docker Desktop` GUI go to the *Containers* tab. You should see now, that there are two containers launched under the group name *i2r*.

    ![Docker Desktop GUI with group of containers running](images/docker_group_running.png)*`Docker Desktop` GUI: Inside the Containers tab you should find the started group of containers.*
    - Another option to inspect the containers is using the terminal. (Note: If you are still in the terminal you executed the *docker compose up*, make sure to start a new terminal, but let the old terminal running.)

        ```bash
        docker ps --all
        ```

        The terminal output will look the following:

        ![Terminal output for *docker ps --all*](images/docker_ps_all.gif)

        There will be information like ID of the container, name of the image the container was build from etc.
8. To connect to the ***i2r-ros-1*** container, the container we will use to work with `ROS`, execute the following command from terminal:

    ```bash
    docker exec -it i2r-ros-1 bash
    ```

    ![GIF: Terminal output for *docker exec -it i2r-ros-1 bash*](images/docker_exec_rosContainer.gif)

    This starts a ***bash* session** in the running *i2r-ros-1* container. ***Bash*** (Bourne-again shell) is a Unix-shell that interacts as a **human-machine interface** with an environment to allow line by line text **in-** and **output**.
9. Now you can test if the *i2r-ros-1* container you started has `ROS` installed and working, by launching ***roscore***. The `ROS` specific terminal command *roscore* launches a collection of *nodes* that are essential to work with a `ROS`-based system. You will learn more about the basics of `ROS` in the [**Introduction to ROS**](../02_IntroductionToROS/) chapter.

    ```bash
    roscore
    ```

    ![GIF: Terminal output for *roscore*](images/roscore.gif)

    If that worked out, you can close the started *ROS Master* by typing *`Ctrl` + `C`*. That shuts down the *Master* node. To leave the *bash* session inside of the *i2r-ros-1 container* just type *`Ctrl` + `D`*.

    ![GIF: Terminal output kill master and leave container session.](images/kill_master_leave_container.gif)
10. To stop the started group of containers named *i2r* you can either use the `Docker Desktop` GUI from the *Container* tab like shown below.

    ![GIF: Terminal output stop container group from GUI.](images/stopping_container_group_from_gui.gif)

    Or switch again to the terminal, where you run the *docker compose up* command and press *`Ctrl` + `C`*.

    ![GIF: Terminal output container group from terminal.](images/stopping_container_group.gif)

    Another option, if that terminal is already closed, is to use the following command:

    ```bash
    docker stop i2r-ros-1 i2r-gui-1
    ```

11. If all the previous steps worked you have successfully installed the envrionment we will be using during in `Introduction to Robotics` course to work with `ROS`.

    If you want to restart a container just make sure that:

    - `Docker Desktop` is running and
    - start the group of containers named *i2r* by using ...
        - GUI or

            ![GIF: Starting container group from GUI.](images/starting_container_group_from_gui.gif)

        - Terminal

            ```bash
            docker start i2r-ros-1 i2r-gui-1
            ```

            ![GIF: Starting container group from Terminal.](images/starting_container_group_from_terminal.gif)

## In case of problems

If you run into any issues during the installation process of these softwares, you are welcome to ask me ([Contact Data](https://www.gut.rwth-aachen.de/cms/Geotechnik/Das-Institut/Team/~peawz/Jan-Holzheim/lidx/1/), via mail, phone or in presence).
